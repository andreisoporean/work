export default [
  {
    id: '0',
    name: 'Layout 0',
    rows: [{ size: 12 }, { size: 6}, { size: 6}, { size: 12 }, { size: 12 }, { size: 4}, { size: 4}, { size: 4}],
    template: 'layouts/l0.png',
  },
  {
    id: '1',
    name: 'Layout 1',
    rows: [{ size: 6}, { size: 6}, { size: 12 }, { size: 12 }, { size: 4}, { size: 4}, { size: 4},{ size: 12 }],
    template: 'layouts/l1.png',
  },
  {
    id: '2',
    name: 'Layout 2',
    rows: [{ size: 4}, { size: 4}, { size: 4 }, { size: 12 }, { size: 12}, { size: 6}, { size: 6},{ size: 12 }],
    template: 'layouts/l2.png',
  },
  {
    id: 3,
    name: 'Layout 3',
    rows: [{ size: 4}, { size: 4}, { size: 4 }, { size: 3 }, { size: 3}, { size: 3}, { size: 3},{ size: 12 }, { size: 12 }, { size: 12 }],
    template: 'layouts/l3.png',
  },
  {
    id: 4,
    name: 'Layout 4',
    rows: [{ size: 6}, { size: 6}, { size: 6 }, { size: 6 }, { size: 4}, { size: 4}, { size: 4},{ size: 12 }, { size: 12 }],
    template: 'layouts/l4.png',
  },
  {
    id: '5',
    name: 'Layout 5',
    rows: [{ size: 6}, { size: 6}, { size: 12 }, { size: 6 }, { size: 6 }, { size: 12 }, { size: 12 }],
    template: 'layouts/l5.png',
  },
  {
    id: 6,
    name: 'Layout 6',
    rows: [{ size: 12}, { size: 12}, { size: 12 }, { size: 12 }, { size: 12 }],
    template: 'layouts/l6.png',
  },
  {
    id: '7',
    name: 'Layout 7',
    rows: [{ size: 6}, { size: 6}, { size: 6 }, { size: 6 }, { size: 6 }, { size: 6 }, { size: 6 }, { size: 6 }, { size: 6 }, { size: 6 }],
    template: 'layouts/l7.png',
  },{
    id: '8',
    name: 'Layout 8',
    rows: [{ size: 4}, { size: 4}, { size: 4 }, { size: 3 }, { size: 3}, { size: 3}, { size: 3},{ size: 6 }, { size: 6 }, { size: 12 }, { size: 12 }],
    template: 'layouts/l8.png',
  }
];
