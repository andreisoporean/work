import React from 'react';
import { Avatar, Button } from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';
import { deepOrange } from '@material-ui/core/colors';
import './index.css';

const UserCard = ({
  id,
  firstName,
  lastName,
  age,
  gender,
}) => {
  const useStyles = makeStyles((theme) => ({
    orange: {
      color: theme.palette.getContrastText(deepOrange[500]),
      backgroundColor: deepOrange[500],
    },
  }));
  const classes = useStyles();

  return (
    <div id={id} className="user-card">
      <div className="top-details">
        <Avatar className={classes.orange}>{firstName.charAt(0)}{lastName.charAt(0)}</Avatar>
      </div>

      <div className="bottom-details">
        <h3>{`${firstName}, ${lastName}`}</h3>
        <p>{`${gender}, ${age}`}</p>
        <Button variant="outlined" color="primary">
          View Details
        </Button>
      </div>
    </div>
  )
}

export default UserCard;