import React from 'react';
import './index.css'

export const Title = () => {
  return (
    <div className='title-wrapper'>
      <h2>This is a title example</h2>
    </div>
  );
}

export const TitleDesc = () => {
  return (
    <div className='title-wrapper'>
      <h2>Hi! I am a title</h2>
      <p>I am a description! Drag me on the right!</p>
    </div>
  );
}
