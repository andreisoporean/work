import React, { useEffect } from 'react';
import DraggableComponent from './draggable-component';
import UserCard from './user-card';
import SimpleTable from './simple-table';
import { useSelector, useDispatch } from 'react-redux';
import { getAllUsers, getUsers } from '../reducers/users-reducer';
import { Title, TitleDesc } from './title';

const ComponentsWrapper = () => {
  const allUsers = useSelector(getUsers);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllUsers())
  }, [dispatch]);

  const hasUsers = allUsers && allUsers.length > 0;

  return (
    <>
      <DraggableComponent id='title2' name='Title With Description'>
        <TitleDesc />
      </DraggableComponent>
      <DraggableComponent id='title' name='Title'>
        <Title />
      </DraggableComponent>
      <DraggableComponent id='table' name='Table'>
        <SimpleTable/>
      </DraggableComponent>
      {hasUsers && allUsers.map((user, index) => {
        const { id, age, firstName, lastName, gender } = user;
        return (
          <DraggableComponent id={id} key={index} name='User Card'>
            <UserCard
              id={id}
              firstName={firstName}
              lastName={lastName}
              age={age}
              gender={gender}
            />
          </DraggableComponent>
        )
      })}
    </>
  );
};

export default ComponentsWrapper;
