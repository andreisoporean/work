import React, { useState } from 'react';
import { Grid, Paper } from '@material-ui/core/';
import './index.css';

const CustomGrid = ({ size, id }) => {
  const [dropped, setDropped] = useState(false);
  const drop = event => {
    event.preventDefault();
    const componentId = event.dataTransfer.getData('componentId');
    const component = document.getElementById(componentId);

    component.style.display = 'block';
    event.target.appendChild(component);
    setDropped(true);
  }

  const dragOver = event => {
    event.preventDefault();
  }
  return (
    <Grid item xs={size} id={id} onDrop={drop} onDragOver={dragOver}>
      <Paper variant="elevation" elevation={3} className={`paper${dropped ? "-active" : "-empty"}`}></Paper>
    </Grid>
  )
}

export default CustomGrid;
