import React, { useState } from 'react';
import './index.css';

const DraggableComponent = ({ children, id, name }) => {
  const [isBeforeDrag, showTitle] = useState(true);
  const dragStart = event => {
    const target = event.target;
    event.dataTransfer.setData('componentId', target.id);
    showTitle(false);
    setTimeout(() => {
      target.style.display = 'none';
    }, 0);
  }

  const dragOver = event => {
    event.stopPropagation();
  }

  return (
    <div
      id={id}
      className={isBeforeDrag ? 'on-drag-wrapper' : 'on-drop-wrapper'}
      onDragStart={dragStart}
      onDragOver={dragOver}
      draggable="true"
    >
      {/* display title only before drag, we want to hide it once the component is dropped */}
      {isBeforeDrag && <p className='title'>{name}</p>}
      {children}
    </div>
  )
}

export default DraggableComponent;
