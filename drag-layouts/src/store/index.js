import { configureStore } from '@reduxjs/toolkit';
import layoutReducer from '../reducers/layout-reducer';
import usersReducer from '../reducers/users-reducer';

export default configureStore({
  reducer: {
    layout: layoutReducer,
    users: usersReducer,
  },
});
