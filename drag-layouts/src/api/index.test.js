import { fetchData } from './index';

jest.mock('./index');

describe('Api | Fetch', () => {
  it('fetches data from server', done => {
    const url = 'https://randomuser.me/api/?results=4';
    fetchData.mockImplementation(() =>
        Promise.resolve([]),
      );
    fetchData(url);

    expect(fetchData).toHaveBeenCalledTimes(1);
    expect(fetchData).toHaveBeenCalledWith('https://randomuser.me/api/?results=4');

    process.nextTick(() => {
      fetchData.mockClear();
      done();
    });
  });
});
