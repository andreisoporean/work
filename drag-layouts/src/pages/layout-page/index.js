import React from 'react';
import { useDispatch } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import layoutCollection from '../../data/mock';
import { setLayout } from '../../reducers/layout-reducer';
import { useHistory } from "react-router-dom";
import './index.css';

const LayoutPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const selectLayout = (layout) => {
    dispatch(setLayout(layout));
    history.push('/configuration')
  }

  const hasData = layoutCollection && layoutCollection.length > 0;

  return (
    <Grid container spacing={3} className="main-grid">
      {hasData && layoutCollection.map(layout => {
        const { name, template, id } = layout;

          return (
            <Grid id={id} className="grid-item" item xs={4}>
              <div onClick={() => selectLayout(layout)} className="grid-content">
                <img src={template} alt={name}/>
              </div>
              <p>{name}</p>
            </Grid>
          )
        }
      )}
    </Grid>
  );
};

export default LayoutPage;
