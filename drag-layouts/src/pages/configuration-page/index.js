import React from 'react';
import { useSelector } from 'react-redux';
import { Grid } from '@material-ui/core/';
import { currentLayout } from '../../reducers/layout-reducer';
import CustomGrid from '../../components/custom-grid';
import ComponentWrapper from '../../components';

import './index.css';

const ConfigurationPage = () => {
  const { rows, name } = useSelector(currentLayout);
  const hasRows = rows && rows.length > 0;

  return (
    <div className="wrapper">
      <div className="left-side">
        <h3>COMPONENTS</h3>
        <Grid container direction="column" justify="center">
          <ComponentWrapper/>
        </Grid>
      </div>
      <div className="right-side">
        <h3>CONFIGURATION SCREEN - {name} </h3>
        <Grid container spacing={4}>
          {hasRows && rows.map(({ size }, index) => (
            <CustomGrid key={index} size={size}/>
          ))}
        </Grid>
      </div>


    </div>
  )
};

export default ConfigurationPage;
