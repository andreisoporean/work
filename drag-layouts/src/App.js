import React from 'react';
import { AppBar, ButtonGroup, Button } from '@material-ui/core/';
import ConfigurationPage from './pages/configuration-page';
import LayoutPage from './pages/layout-page';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const App = () => {
  return (
    <div className="App">
      <Router>
        <AppBar className="top-bar" color="secondary" position="static">
          <ButtonGroup className="menu" variant="text" color="inherit" aria-label="text primary button group">
            <Button>
              <Link className="menu-button" to="/">Layouts</Link>
            </Button>
            <Button>
              <Link className="menu-button" to="/configuration">Configuration</Link>
            </Button>
          </ButtonGroup>
        </AppBar>
        <Switch>
          <Route path="/configuration">
            <ConfigurationPage />
          </Route>
          <Route path="/">
            <LayoutPage />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
