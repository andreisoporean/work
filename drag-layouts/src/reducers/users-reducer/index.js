import { createSlice } from '@reduxjs/toolkit';
import { serviceGetUsers } from '../../services/users';

export const usersSlice = createSlice({
  name: 'users',
  initialState: {
    allUsers: [],
  },
  reducers: {
    setUsers: (state, { payload }) => {
      return {
        ...state,
        allUsers: payload,
      };
    },
  },
});

export const getAllUsers = () => async dispatch => {
  const users = await serviceGetUsers();

  dispatch(setUsers(users));
}

export const { setUsers } = usersSlice.actions;

export const getUsers = ({ users: { allUsers } }) => allUsers;

export default usersSlice.reducer;
