import { createSlice } from '@reduxjs/toolkit';
import layoutCollection from '../../data/mock';

// if no layout is selected and user clicks on configuration button,
// we need to set a default layout to be edited
const defaultLayout = layoutCollection[0];

export const layoutSlice = createSlice({
  name: 'layout',
  initialState: {
    selectedLayout: defaultLayout,
  },
  reducers: {
    setLayout: (state, { payload }) => {
      return {
        ...state,
        selectedLayout: payload,
      };
    },
  },
});

export const { setLayout } = layoutSlice.actions;

export const currentLayout = ({ layout: { selectedLayout } }) => selectedLayout;

export default layoutSlice.reducer;
