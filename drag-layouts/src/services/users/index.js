import { fetchData } from '../../api/index';
import { transformUsers } from '../../transformers';

const API_URL = 'https://randomuser.me/api/?results=4';

export const serviceGetUsers = async () => {
  const users = await fetchData(API_URL);
  const processedUsers = transformUsers(users);

  return processedUsers;
}
