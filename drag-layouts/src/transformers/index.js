// we don't need everything from the api response, so we are stripping some data out
// and put into global state only what we need
export const transformUsers = users => {
  const tidyUsers = users.reduce((acc,
    {
      gender,
      name: { first, last },
      registered: { date },
      dob: { age } }) => [
    ...acc,
    {
      id: date,
      firstName: first,
      lastName: last,
      gender,
      age,
    }
  ], []);

  return tidyUsers;
}
