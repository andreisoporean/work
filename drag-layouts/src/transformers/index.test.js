import { transformUsers } from './index';

describe('Transformers | Transform users', () => {
  const users = [
    {
      cell: '001-1233',
      dob: { date: '1999-01-03', age: 23 },
      email: 'email',
      gender: 'male',
      id: {},
      location: {},
      login: {},
      name: {
        title: 'Mr',
        first: 'John',
        last: 'Doe',
      },
      registered: { date: '2008-09-09'},
    },
    {
      cell: '111-4444',
      dob: { date: '1989-01-03', age: 13 },
      email: 'email',
      gender: 'female',
      id: {},
      location: {},
      login: {},
      name: {
        title: 'Ms',
        first: 'Jane',
        last: 'Wilson',
      },
      registered: { date: '2012-09-09'},
    }
  ];

  const expectedUsers = [
    {
      id: '2008-09-09',
      firstName: 'John',
      lastName: 'Doe',
      gender: 'male',
      age: 23,
    },
    {
      id: '2012-09-09',
      firstName: 'Jane',
      lastName: 'Wilson',
      gender: 'female',
      age: 13,
    }
  ]

  it('should return a users array with only used data', () => {
    const tidyUsers = transformUsers(users);
    expect(tidyUsers).toEqual(expectedUsers);
  });
});
